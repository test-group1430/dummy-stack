from constructs import Construct
from aws_cdk import (
    Duration,
    NestedStack,
    aws_iam as iam,
    aws_sqs as sqs,
    aws_sns as sns,
    aws_sns_subscriptions as subs,
)


class Web(NestedStack):
    def __init__(self, scope: Construct, id: str, test_env, stage, **kwargs) -> None:
        super().__init__(scope, id, **kwargs)

        queue = sqs.Queue(
            self,
            "DummyStackQueue",
            visibility_timeout=Duration.seconds(300),
        )

        topic = sns.Topic(self, "DummyStackTopic")

        topic.add_subscription(subs.SqsSubscription(queue))
