from aws_cdk import (
    Stack,
)

from dummy_stack import Web


class Base(Stack):
    def __init__(self, scope, id, env, tags, test_env, stage):
        super().__init__(scope, id, env=env, tags=tags)

        self.web = Web(self, id=f"{id}-web", test_env=test_env, stage=stage)
