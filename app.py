#!/usr/bin/env python3

import aws_cdk as cdk
import yaml

from dummy_stack.dummy_stack_stack import DummyStackStack
from dummy_stack import Base


app = cdk.App()

env_file = app.node.try_get_context("env")

print(f"Env: {env_file}")

with open(env_file, "r") as stream:
    test_env = yaml.safe_load(stream)


env = cdk.Environment(account=test_env["account"], region=test_env["region"])
tags = {"Project": test_env["project"]}

stage = test_env["stages"]
print(f"Test env: {stage}")


for stage in test_env["stages"]:
    base = Base(
        scope=app,
        id=f"{test_env['project']}-{stage['name']}",
        env=env,
        tags=tags,
        test_env=test_env,
        stage=stage,
    )


app.synth()
